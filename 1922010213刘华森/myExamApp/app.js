'use strtic';

let Koa=require('koa');
let bodyParser=require('koa-bodyparser');
let controller=require('./controllers');


let tem=require("./templating");
let static=require('koa-static');
let app=new Koa();

app.use(static(__dirname))
app.use(bodyParser());
app.use(tem)
app.use(controller());



let port=3008;
app.listen(port);
console.log(`http://127.0.0.1:${port}`);
